import './App.css';
import NavBar from './components/navbar/NavBar'
import Card from "./components/card/Card"
import Submission from "./components/submission/Submission"

function App() {
  return (
    <>
      <NavBar />
      Classes
      <div>Hero Image</div>
      <Card name="Mt. Rushmore" location="SD"/>
      <Card name="Redwood National Forest" location="CA"/>
      <Submission />
        <div>Footer</div>
      
      Hello, I am the travel app.
    </>
  );
}

export default App;
