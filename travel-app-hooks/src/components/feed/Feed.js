import React from "react";
import { v4 } from "uuid";
import "./Feed.css";
import { Card } from "../";

export const Feed = ({ submissions, setSubmissions }) => {
  return (
    <div className="feed">
      {submissions.map((el, i) => {
        const feedData = {
          name: el.name,
          location: el.location,
          index: i,
          submissions: submissions,
          setSubmissions: setSubmissions,
          id: el.id,
        };
        return <Card data={feedData} key={v4()} />;
      })}
    </div>
  );
};
