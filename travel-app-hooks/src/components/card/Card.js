import React, { useState } from "react";
import "./Card.css";
import { Title } from "../";
import { v4 } from "uuid";

export const Card = ({ data: { name, location, index, submissions, setSubmissions, id } }) => {
  const [choice, setChoice] = useState(index + 1);
  const deleteSelf = () => {
    setSubmissions([...submissions.slice(0, index), ...submissions.slice(index + 1)]);
  };

  const reorderCards = (chosenSpot) => {
    if (chosenSpot - 1 < index) {
      setSubmissions([
        ...submissions.slice(0, chosenSpot - 1),
        submissions[index],
        ...submissions.slice(chosenSpot - 1, index),
        ...submissions.slice(index + 1),
      ]);
    } else {
      setSubmissions([
        ...submissions.slice(0, index),
        ...submissions.slice(index + 1, chosenSpot),
        submissions[index],
        ...submissions.slice(chosenSpot),
      ]);
    }
  };

  const handleChange = (e) => {
    setChoice(e.target.value);
    reorderCards(e.target.value);
  };

  return (
    <div className="card">
      <select value={choice} onChange={(e) => handleChange(e)}>
        {submissions.map((el, i) => (
          <option value={i + 1} key={v4()}>
            {i + 1}
          </option>
        ))}
      </select>
      <button onClick={deleteSelf}>X</button>
      <Title name={name} location={location} />
      <div>Description</div>
      <div>
        <ul>
          <li>Address</li>
          <li>Hours</li>
          <li>Contact</li>
        </ul>
      </div>
    </div>
  );
};
