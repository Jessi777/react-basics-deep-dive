import React from "react";

export const Submission = ({ data: { name, setName, location, setLocation, submissions, setSubmissions } }) => {
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(name, location);
    setSubmissions([...submissions, { name, location }]);
    setName("");
    setLocation("");
  };

  return (
    <form onSubmit={handleSubmit}>
      <h5>You've contributed destinations</h5>
      <label htmlFor="name">Name:</label>
      <input name="name" value={name} onChange={(event) => setName(event.target.value)} />
      <label htmlFor="location">Location:</label>
      <input name="location" value={location} onChange={(event) => setLocation(event.target.value)} />
      <button type="submit">Add to list</button>
    </form>
  );
};
