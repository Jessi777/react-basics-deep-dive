import React, { useState } from "react";
import "./App.css";
import { NavBar, Submission, Feed } from "./components";

function App() {
  const [name, setName] = useState("");
  const [location, setLocation] = useState("");
  const [submissions, setSubmissions] = useState([]);

  const submissionData = {
    name: name,
    setName: setName,
    location: location,
    setLocation: setLocation,
    submissions: submissions,
    setSubmissions: setSubmissions,
  };

  const deleteAll = () => {
    setSubmissions([]);
  };

  return (
    <>
      <NavBar />
      hooks
      <div>Hero Image</div>
      <Feed submissions={submissions} setSubmissions={setSubmissions} />
      <Submission data={submissionData} />
      <button onClick={deleteAll}>nuke it</button>
      <div>Footer</div>
    </>
  );
}

export default App;
